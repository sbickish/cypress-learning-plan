// NOTE Assignment 1 using math operators simply and reassigning them as well as with a for loop

let sum = 5 /* here were assigning sum to 5*/ 
sum += 10 /** here im adding 10 to the original value of 5 output is 15*/

let subtraction = 10 /**here is the same set up but with subtraction */
subtraction -= 7  /**output is 3 */


let multiply = (sum * subtraction) /**here we are multiplying the two past variables output. multiplication output is 45 */


let divide = (multiply / sum) /**this output is 3 */


let modulus = (multiply % sum) /**here the output is 0 because there is no remainder */

let array = [1,2,3,4]

function addItAllUp(array){
    let total = 0
    for(let i = 0; i < array.length; i++){
        total += array[i]
    }
    return total
}
addItAllUp(array) /**here i have declared an array and used it as a parameter for my forloop to iterate over. on each iteration it adds the current iteration index to the total variable. once finished i return the total. output is 10 */

// NOTE assignment 2 declaring variables

// there are 3 ways to declare variables let, const, var
let apple = ['apple'] /**let allows a variable to change its value an example would be my addition on line 3 and 4 */

const orange = ['orange'] /**const variables are constant. meaning the variable will not change value.  */

var banana = ['banana'] /**var variables are known for causing a behavior called hoisting which can cause scope issues. they have their place in code but the let declaration is more commonly used */

// NOTE assignment 3 creating nested functions. 

let list = [2,4,6,8]

function nestedAddition(list){
    let total = 0
    for(let i = 0; i < list.length; i ++){
        total += list[i]
    }
    let sum = 5
    function plus(){
        total += sum
    }
    plus()
    return total
   
    }
    nestedAddition(list) /**here i have a for loop iterating over the list array and on every iteration im adding it to a varable named total. outside of the for loop i have another variable named sum and assigned it to 5. i have a nested function called plus where i added the variable total from the top level function and the variable sum. after that i called the plus function and returned the total which outputs 25. i only needed to call the nestedfunction to get the result. */

    // NOTE assignment 4 age calculator

     function getAge(){
         window.event.preventDefault()
         let userMonth = document.getElementById('month').value /**here in grabbing the value inputed from the user */
         let userDay = document.getElementById('day').value
         let userYear = document.getElementById('year').value
        
         
  let date = new Date(); /**here im getting the current day, month, year and setting them to variables  */
  let currentMonth = 1 + date.getMonth();
  let month = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  let currentDay = date.getDate();
  let currentYear = date.getFullYear();

  if(userDay > currentDay){ /**here are some conditionals that, depending on the users birthdate day and month they inputed will get the accurate result. otherwise it will just simply subtract current date from birthdate. same as below with month and year */
    currentDay = currentDay + month[currentMonth - 1];
    currentMonth = currentMonth - 1;
  }
  if(userMonth > currentMonth){
    currentMonth = currentMonth + 12;
    currentYear = currentYear - 1;
  }
  let m = currentMonth - userMonth; /**here is a simple subtraction of the current date to the users birthdate */
  let d = currentDay - userDay;
  let y = currentYear - userYear;
  document.getElementById('result').innerText = 'Your Age is '+y+' Years '+m+' Months '+d+' Days';

     }
        getAge()

        // NOTE assignments 6 and 7 i will write a javascript class with a constructor to simulate a front end model for cars.

        class Cars{ /**here is an instance of a class which is a template or blueprint for an Object. in this case this is a car object of what a car is or should look like when rendered to the page. the constructor is always loaded first. this is particularly useful when utilized in a controller to render data to the page before other functions run. */
            constructor(data) {
                this.make = data.make
                this.model = data.model
                this.year = data.year
                this.price = data.price
                this.color = data.color
                this.description = data.description
                this.imgUrl = data.imgUrl
            }
        }

